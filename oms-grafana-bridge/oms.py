import itertools
import os
import urllib.parse
from datetime import datetime, timezone

from dateutil import parser
from omsapi import OMSAPI, OMS_FILTER_OPERATORS
OMS_FILTER_OPERATORS.append("CT") # Add missing "ConTain" operator

CLIENT_ID = os.environ["OMS_GRAFANA_BRIDGE_CLIENT_ID"]
CLIENT_SECRET = os.environ["OMS_GRAFANA_BRIDGE_CLIENT_SECRET"]

omsapi = OMSAPI("https://cmsoms.cern.ch/agg/api", "v1", cert_verify=False, verbose=False)
omsapi.auth_oidc(CLIENT_ID, CLIENT_SECRET)

def get_run(run_number):
    query = omsapi.query("runs")
    query.filter("run_number", run_number, "EQ")

    response = query.data().json()

    if len(response["data"]) == 1:
        return {"result": "ok", "data": response["data"][0]["attributes"]}
    else:
        return {"result": "nok"}

def get_lumisections(run_number):
    query = omsapi.query("lumisections")
    query.filter("run_number", run_number)

    query.sort("lumisection_number", asc=True)
    query.attrs(["lumisection_number", "start_time", "end_time", "delivered_lumi_per_lumisection", "recorded_lumi_per_lumisection"])

    rows = list()
    for page in itertools.count(start=1):
        response = query.paginate(page=page, per_page=1000).data().json()

        for data in response["data"]:
            row = data["attributes"]
            row["start_time"] = int(parser.parse(row["start_time"]).timestamp()*1000)
            row["end_time"] = int(parser.parse(row["end_time"]).timestamp()*1000)
            rows.append(row)

        if response["links"]["next"] is None:
            break

    return rows

def _get_fills(from_timestamp, to_timestamp):
    query = omsapi.query("fills")

    query.filter("start_time", urllib.parse.quote_plus(datetime.fromtimestamp(to_timestamp, timezone.utc).isoformat()), "LT")
    query.filter("end_time", urllib.parse.quote_plus(datetime.fromtimestamp(from_timestamp, timezone.utc).isoformat()), "GE")

    query.sort("fill_number", asc=True)
    query.attrs(["fill_number", "start_time", "end_time"])

    rows = list()
    for page in itertools.count(start=1):
        response = query.paginate(page=page, per_page=1000).data().json()

        for data in response["data"]:
            row = data["attributes"]
            row["start_time"] = int(parser.parse(row["start_time"]).timestamp()*1000)
            row["end_time"] = int(parser.parse(row["end_time"]).timestamp()*1000)
            row["text"] = f"Fill {row['fill_number']}"
            rows.append(row)

        if response["links"]["next"] is None:
            break

    return rows

def _get_luminosity(from_timestamp, to_timestamp):
    query = omsapi.query("diplogger")

    query.filter("source_dir", "dip/CMS/BRIL/Luminosity", "EQ")
    query.filter("dip_time", urllib.parse.quote_plus(datetime.fromtimestamp(from_timestamp, timezone.utc).isoformat()), "GE")
    query.filter("dip_time", urllib.parse.quote_plus(datetime.fromtimestamp(to_timestamp, timezone.utc).isoformat()), "LT")

    query.custom("group[count]", 2000)

    query.sort("first_dip_time")
    query.attrs(["first_dip_time", "inst_lumi"])

    response = query.paginate(page=1, per_page=2500).data().json()

    rows = list()
    for data in response["data"]:
        row = data["attributes"]
        row["first_dip_time"] = int(parser.parse(row["first_dip_time"]).timestamp()*1000)
        rows.append(row)

    return rows

def _get_magnet(from_timestamp, to_timestamp):
    query = omsapi.query("conditions")

    query.filter("source_dir", "CMS/MAGNET", "EQ")
    query.filter("change_date", urllib.parse.quote_plus(datetime.fromtimestamp(to_timestamp, timezone.utc).isoformat()), "LT")
    query.filter("change_date", urllib.parse.quote_plus(datetime.fromtimestamp(from_timestamp, timezone.utc).isoformat()), "GE")

    query.custom("group[count]", 2000)

    query.sort("first_change_date")
    query.attrs(["first_change_date", "magnet_field"])

    rows = list()
    for page in itertools.count(start=1):
        response = query.paginate(page=page, per_page=2500).data().json()

        for data in response["data"]:
            row = data["attributes"]
            row["first_change_date"] = int(parser.parse(row["first_change_date"]).timestamp()*1000)
            rows.append(row)

        if response["links"]["next"] is None:
            break

    return rows

def _get_runs(from_timestamp, to_timestamp):
    query = omsapi.query("runs")

    query.filter("components", "GEM", "CT")
    query.filter("start_time", urllib.parse.quote_plus(datetime.fromtimestamp(to_timestamp, timezone.utc).isoformat()), "LT")
    query.filter("end_time", urllib.parse.quote_plus(datetime.fromtimestamp(from_timestamp, timezone.utc).isoformat()), "GE")

    query.sort("run_number", asc=True)
    query.attrs(["run_number", "start_time", "end_time"])

    rows = list()
    for page in itertools.count(start=1):
        response = query.paginate(page=page, per_page=1000).data().json()

        for data in response["data"]:
            row = data["attributes"]
            row["start_time"] = int(parser.parse(row["start_time"]).timestamp()*1000)
            row["end_time"] = int(parser.parse(row["end_time"]).timestamp()*1000)
            row["text"] = f"Run {row['run_number']}"
            rows.append(row)

        if response["links"]["next"] is None:
            break

    return rows

RESOURCES = {
    "fills": _get_fills,
    "luminosity": _get_luminosity,
    "magnet": _get_magnet,
    "runs": _get_runs,
}
