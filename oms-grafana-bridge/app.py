import json

from dateutil import parser
from flask import Flask, jsonify, request

import oms

app = Flask(__name__)

@app.route("/")
def _root():
    return "ok"

@app.route("/metrics", methods=["POST"])
def _metrics():
    data = request.json
    # print(json.dumps(data, indent=4))

    return jsonify(list(oms.RESOURCES.keys()))

@app.route("/variable", methods=["POST"])
def _variable():
    data = request.json
    # print(json.dumps(data, indent=4))

    return jsonify([])

@app.route("/query", methods=["POST"])
def _query():
    data = request.json
    # print(json.dumps(data, indent=4))

    from_timestamp = parser.parse(data["range"]["from"]).timestamp()
    to_timestamp = parser.parse(data["range"]["to"]).timestamp()

    reply = list()
    for target in data["targets"]:
        resource = target["target"]

        values = oms.RESOURCES[resource](from_timestamp, to_timestamp)
        if not values:
            continue

        table = {
            "type": "table",
            "columns": list(),
            "rows": list(),
        }

        for key in values[0].keys():
            table["columns"].append({"text": key})
        for value in values:
            table["rows"].append(list(value.values()))

        reply.append(table)

    return jsonify(reply)

@app.route("/easy/run", methods=["POST"])
def _easy_run_query():
    data = request.json
    # print(json.dumps(data, indent=4))

    run_number = int(data["run-number"])
    reply = oms.get_run(run_number)

    return jsonify(reply)

@app.route("/easy/lumisections", methods=["POST"])
def _easy_lumisections_query():
    data = request.json
    # print(json.dumps(data, indent=4))

    run_number = int(data["run-number"])
    reply = oms.get_lumisections(run_number)

    return jsonify(reply)
