import datetime
import multiprocessing.pool
import os
import socket
import struct

from dateutil import parser
from flask import Flask, jsonify, request
from flask.json.provider import JSONProvider
from redis import Redis
import numpy as np
import orjson

import sql

pool = multiprocessing.pool.ThreadPool(processes = 16)

REDIS_HOST = os.environ.get("DCS_DB_GRAFANA_BRIDGE_REDIS_HOST", "localhost")
cache_client = Redis(host=REDIS_HOST,
                     socket_keepalive=True,
                     socket_keepalive_options={socket.TCP_KEEPIDLE: 30,
                                               socket.TCP_KEEPCNT: 2,
                                               socket.TCP_KEEPINTVL: 30},
                     health_check_interval=120)

class ORJSONProvider(JSONProvider):
    option = orjson.OPT_APPEND_NEWLINE | orjson.OPT_NAIVE_UTC | orjson.OPT_SERIALIZE_NUMPY

    def dumps(self, obj, **kwargs):
        return orjson.dumps(obj, option=self.option).decode("utf-8")

    def loads(self, string, **kwargs):
        return orjson.loads(string)

    def response(self, *args, **kwargs):
        obj = self._prepare_response_obj(args, kwargs)
        return self._app.response_class(
            orjson.dumps(obj, option=orjson.OPT_SERIALIZE_NUMPY),
            mimetype="application/json"
        )

class WeeklyBucket:
    SECONDS_IN_WEEK = datetime.timedelta(days = 7).total_seconds()

    def __init__(self, timestamp):
        self.start = timestamp // self.SECONDS_IN_WEEK * self.SECONDS_IN_WEEK

    def end(self):
        return self.start + self.SECONDS_IN_WEEK

    def next(self):
        return WeeklyBucket(self.end())

def _fetch_targets(args):
    def _fetch_single_target(from_timestamp, to_timestamp, metric, tag):
        buckets = []
        bucket = WeeklyBucket(from_timestamp)
        while True:
            buckets.append(bucket)
            bucket = bucket.next()
            if bucket.start >= to_timestamp:
                break

        datapoints = np.empty(shape=(0,2))
        for bucket in buckets:
            cache_key = f"{metric},{tag}@{int(bucket.start)}"

            # Try to fetch the data from the cache
            cache_val = cache_client.get(cache_key)
            if cache_val is not None:
                length = struct.unpack('>I', cache_val[:4])[0]
                value = np.frombuffer(cache_val[4:]).reshape(length, 2)
                datapoints = np.concatenate((datapoints, value))
                continue

            # Otherwise, fetch data from the source
            value = np.array(sql.fetch_data(bucket.start, bucket.end(), metric, tag))
            if len(value):
                datapoints = np.concatenate((datapoints, value))

            # Cache only closed buckets
            if bucket.end() <= datetime.datetime.now().timestamp():
                cache_val = struct.pack('>I', value.shape[0]) + value.tobytes()
                cache_client.set(cache_key, cache_val, ex = int(WeeklyBucket.SECONDS_IN_WEEK * 4))

        # The in-bucket data may cover a range larger than requested
        window_selector = (from_timestamp <= datapoints[:, 0]) & (datapoints[:, 0] < to_timestamp)

        # Select the last point out-of-range to enhance Grafana presentation
        last_selector = (datapoints[:, 0] < from_timestamp)
        last_selector[:-1] ^= last_selector[1:]

        datapoints = datapoints[window_selector | last_selector]

        # Assign the last point out-of-range to the beginning of the time range
        has_last = np.any(last_selector)
        if (has_last and not np.any(window_selector)) or (has_last and (np.argmax(last_selector) != np.argmax(window_selector))):
            datapoints[0, 0] = from_timestamp

        # Adjust to Grafana/JSON expectations
        datapoints[:, 0] = (datapoints[:, 0] * 1000).astype(int)
        datapoints = datapoints[:, [1, 0]]

        return{
            "target": f"{metric},{tag}",
            "datapoints": np.ascontiguousarray(datapoints)
        }

    reply = list()
    for result in pool.starmap(_fetch_single_target, args):
        reply.append(result)

    return jsonify(reply)

app = Flask(__name__)
app.json = ORJSONProvider(app)

@app.route("/")
def _root():
    return "ok"

@app.route("/metrics", methods=["POST"])
def _metrics():
    data = request.json
    # print(orjson.dumps(data, option=orjson.OPT_INDENT_2))

    return jsonify(list(sql.METRICS.keys()))

@app.route("/variable", methods=["POST"])
def _variable():
    data = request.json
    # print(orjson.dumps(data, option=orjson.OPT_INDENT_2))

    measurements = list()
    for metric in data["payload"]:
        measurements += [f"{metric},{tag}" for tag in sql.METRICS[metric].keys()]

    rv = [{'__text': key, '__value': key} for key in measurements]

    return jsonify(rv)

@app.route("/query", methods=["POST"])
def _query():
    data = request.json
    # print(orjson.dumps(data, option=orjson.OPT_INDENT_2))

    from_timestamp = parser.parse(data["range"]["from"]).timestamp()
    to_timestamp = parser.parse(data["range"]["to"]).timestamp()

    args = []
    for target in data["targets"]:
        metric = target["target"]
        for tag in target["payload"]:
            args.append((from_timestamp, to_timestamp, metric, tag))

    return _fetch_targets(args)

@app.route("/easy/query", methods=["POST"])
def _easy_query():
    data = request.json
    # print(orjson.dumps(data, option=orjson.OPT_INDENT_2))

    from_timestamp = float(data["from"])
    to_timestamp = float(data["to"])

    args = []
    for target in data["targets"]:
        args.append((from_timestamp, to_timestamp, target["metric"], target["tag"]))

    return _fetch_targets(args)
