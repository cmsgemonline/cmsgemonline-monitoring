wsgi_app = 'app:app'

# For development purposes
bind = '0.0.0.0:5000'
reload = True

# Reasonable concurrency defaults
workers = 1
threads = 16

# Logging
accesslog = '-'
access_log_format = '%(h)s %(l)s %(u)s %(t)s "%(r)s" %(s)s %(b)s "%(f)s" "%(a)s" "%(D)s"'
