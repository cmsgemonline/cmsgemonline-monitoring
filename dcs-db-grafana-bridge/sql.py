import datetime
import os

import oracledb

# Thick mode
TNSNAMES_DIR = os.environ.get("DCS_DB_GRAFANA_BRIDGE_TNSNAMES", "/etc")
oracledb.init_oracle_client(config_dir=TNSNAMES_DIR)

DB_CREDENTIALS = os.environ["DCS_DB_GRAFANA_BRIDGE_DB"]

def fetch_data(from_timestamp, to_timestamp, metric, tag):
    query = METRICS[metric][tag]

    db = oracledb.connect(DB_CREDENTIALS)

    cursor = db.cursor()
    cursor.execute(query, {
        # Convert the timestamps to UTC naive datetimes
        "var_from_date": datetime.datetime.fromtimestamp(from_timestamp, datetime.timezone.utc).replace(tzinfo=None),
        "var_to_date": datetime.datetime.fromtimestamp(to_timestamp, datetime.timezone.utc).replace(tzinfo=None),
    })

    # Convert the UTC naive datetimes to timestamps
    rows = [(row[0].replace(tzinfo=datetime.timezone.utc).timestamp(), row[1]) for row in cursor]

    # for row in rows:
    #     print(row)

    return rows

LV_HV_QUERY = """
-- Build a table with the logical Dp mapping
WITH logical_dp_map as
(
    SELECT
        logical_dp.DPNAME as logical_dp_name,
        DECODE(config.UPDATEID, MIN(config.UPDATEID) OVER(), TO_DATE(NULL), config.CHANGE_DATE) as start_date,
        LEAD(config.CHANGE_DATE) OVER (PARTITION BY logical_dp.DPNAME ORDER BY config.UPDATEID) as end_date,
        config.{in_config_column_name} as hardware_dp_name

    FROM CMS_GEM_PVSS_COND.{in_config_table_name} config

    JOIN CMS_GEM_PVSS_COND.DP_NAME2ID logical_dp
        ON config.DPID = logical_dp.ID

    WHERE logical_dp.DPNAME = '{in_logical_dp_name}' AND config.{in_config_column_name} IS NOT NULL
)

-- Select data within the time window
SELECT
    -- logical_dp_map.logical_dp_name AS logical_dp_name,
    -- hardware_dp.DPNAME AS hardware_dp_name,
    event.CHANGE_DATE AS timestamp,
    event.{in_event_column_name} as value

FROM CMS_GEM_PVSS_COND.{in_event_table_name} event

JOIN CMS_GEM_PVSS_COND.DP_NAME2ID hardware_dp
    ON event.DPID = hardware_dp.ID

JOIN logical_dp_map
    ON hardware_dp.DPNAME = 'cms_gem_dcs_1:' || logical_dp_map.hardware_dp_name
    AND (event.CHANGE_DATE >= logical_dp_map.start_date OR logical_dp_map.start_date IS NULL)
    AND (event.CHANGE_DATE < logical_dp_map.end_date OR logical_dp_map.end_date IS NULL)

WHERE
    event.{in_event_column_name} IS NOT NULL
    AND event.CHANGE_DATE >= :var_from_date
    AND event.CHANGE_DATE < :var_to_date

ORDER BY event.CHANGE_DATE ASC
"""

def _format_lv_hv_query(*params):
    formatted_query = LV_HV_QUERY.format(
        in_logical_dp_name = params[0],
        in_config_table_name = params[1],
        in_config_column_name = params[2],
        in_event_table_name = params[3],
        in_event_column_name = params[4],
    )
    return formatted_query

def _fill_lv_metrics(column):
    tags = dict()

    # GE1/1
    for ichamber in range(1, 37):
        for ilayer in range(1,3):
            tags[f"GE11-M-{ichamber:02}L{ilayer}:LV"] = _format_lv_hv_query(f"cms_gem_dcs_1:GE11_m_{ichamber:02}", "GEMCHAMBERCONFIGGE11", f"LAYER{ilayer}_LV_LVCHDP", "FWCAENCHANNEL", column)
            tags[f"GE11-P-{ichamber:02}L{ilayer}:LV"] = _format_lv_hv_query(f"cms_gem_dcs_1:GE11_p_{ichamber:02}", "GEMCHAMBERCONFIGGE11", f"LAYER{ilayer}_LV_LVCHDP", "FWCAENCHANNEL", column)

    # GE2/1
    for iendcap, ichamber, ilayer, idcslayer in (("P", 16, 1, 2), ("M", 2, 1, 1), ("M", 3, 1, 1), ("M", 4, 1, 1), ("M", 16, 1, 1), ("M", 18, 1, 1)):
        idcsendcap = iendcap.lower()
        for imodule in ("A", "B", "C", "D"):
            idcsmodule = ord(imodule) - ord("A") + 1
            tags[f"GE21-{iendcap}-{ichamber:02}L{ilayer}{imodule}:LV"] = _format_lv_hv_query(f"cms_gem_dcs_1:GE21_{idcsendcap}_{ichamber:02}", "GEMCHAMBERCONFIGGE21", f"LAYER{idcslayer}_MODULE{idcsmodule}_LV_LVCHDP", "FWCAENCHANNEL", column)

    return tags

def _fill_hv_metrics(column):
    tags = dict()

    # GE1/1
    for ichamber in range(1, 37):
        for ilayer in range(1,3):
            for ielectrode in ["DRIFT", "G1TOP", "G1BOT", "G2TOP", "G2BOT", "G3TOP", "G3BOT"]:
                tags[f"GE11-M-{ichamber:02}L{ilayer}:HV:{ielectrode}"] = _format_lv_hv_query(f"cms_gem_dcs_1:GE11_m_{ichamber:02}", "GEMCHAMBERCONFIGGE11", f"LAYER{ilayer}_{ielectrode}_HVCHDP", "FWCAENCHANNELA1515", column)
                tags[f"GE11-P-{ichamber:02}L{ilayer}:HV:{ielectrode}"] = _format_lv_hv_query(f"cms_gem_dcs_1:GE11_p_{ichamber:02}", "GEMCHAMBERCONFIGGE11", f"LAYER{ilayer}_{ielectrode}_HVCHDP", "FWCAENCHANNELA1515", column)

    # GE2/1
    for iendcap, ichamber, ilayer, idcslayer in (("P", 16, 1, 2), ("M", 2, 1, 1), ("M", 3, 1, 1), ("M", 4, 1, 1), ("M", 16, 1, 1), ("M", 18, 1, 1)):
        idcsendcap = iendcap.lower()
        for imodule in ("A", "B", "C", "D"):
            idcsmodule = ord(imodule) - ord("A") + 1
            for ielectrode in ["DRIFT", "G1TOP", "G1BOT", "G2TOP", "G2BOT", "G3TOP", "G3BOT"]:
                tags[f"GE21-{iendcap}-{ichamber:02}L{ilayer}{imodule}:HV:{ielectrode}"] = _format_lv_hv_query(f"cms_gem_dcs_1:GE21_{idcsendcap}_{ichamber:02}", "GEMCHAMBERCONFIGGE21", f"LAYER{idcslayer}_MODULE{idcsmodule}_{ielectrode}_HVCHDP", "FWCAENCHANNELA1515", column)

    return tags

RACK_QUERY = """
-- Select data within the time window
SELECT
    event.CHANGE_DATE AS timestamp,
    event.{in_event_column_name} as value

FROM CMS_GEM_PVSS_COND.GEMDIPRACK event

JOIN CMS_GEM_PVSS_COND.DP_NAME2ID dp
    ON event.DPID = dp.ID

WHERE
    dp.DPNAME = '{in_dp_name}'
    AND event.{in_event_column_name} IS NOT NULL
    AND event.CHANGE_DATE >= :var_from_date
    AND event.CHANGE_DATE < :var_to_date

ORDER BY event.CHANGE_DATE ASC
"""

RACK_LIST = [
    "S1F12",
    "S1F13",
    "S1G12",
    "S1G13",
    "S1H12",
    "S1H13",
    "S2E01",
    "S2E02",
    "S2E03",
    "S4F03",
    "X5V33",
    "X5J33",
    "X2V33",
    "X2J33",
]

def _format_rack_query(*params):
    formatted_query = RACK_QUERY.format(
        in_dp_name = params[0],
        in_event_column_name = params[1],
    )
    return formatted_query

def _fill_rack_metrics():
    tags = dict()

    for rack in RACK_LIST:
        tags[f"{rack}:TEMPERATURE1"] = _format_rack_query(f"cms_gem_dcs_1:{rack}", "TEMPERATURE1")
        tags[f"{rack}:TEMPERATURE2"] = _format_rack_query(f"cms_gem_dcs_1:{rack}", "TEMPERATURE2")
        tags[f"{rack}:TURBINECURRENT1"] = _format_rack_query(f"cms_gem_dcs_1:{rack}", "TURBINECURRENT1")
        tags[f"{rack}:TURBINECURRENT2"] = _format_rack_query(f"cms_gem_dcs_1:{rack}", "TURBINECURRENT2")

    return tags

FOS_QUERY = """
-- Select data within the time window
SELECT
    event.CHANGE_DATE AS timestamp,
    event.{in_event_column_name} as value

FROM CMS_GEM_PVSS_COND.{in_event_table_name} event

JOIN CMS_GEM_PVSS_COND.DP_NAME2ID dp
    ON event.DPID = dp.ID

WHERE
    dp.DPNAME = '{in_dp_name}'
    AND event.{in_event_column_name} IS NOT NULL
    AND event.CHANGE_DATE >= :var_from_date
    AND event.CHANGE_DATE < :var_to_date

ORDER BY event.CHANGE_DATE ASC
"""

def _format_fos_query(*params):
    formatted_query = FOS_QUERY.format(
        in_dp_name = params[0],
        in_event_table_name = params[1],
        in_event_column_name = params[2],
    )
    return formatted_query

def _fill_fos_metrics():
    tags = dict()

    # Old GE1/1 Dps -- limited to odd (i.e. short) chambers
    for ichamber in range(1, 37, 2):
        for ilayer in range(1,3):
            tags[f"GE11-M-{ichamber:02}L{ilayer}:TEMP:OLD:VALUE"] = _format_fos_query(f"cms_gem_dcs_1:GE11_m_{ichamber:02}_FOS", "GEMDIPFOS", f"L{ilayer}TEMP")
            tags[f"GE11-M-{ichamber:02}L{ilayer}:TEMP:OLD:VALID"] = _format_fos_query(f"cms_gem_dcs_1:GE11_m_{ichamber:02}_FOS", "GEMDIPFOS", f"L{ilayer}TEMPVALIDITY")

            tags[f"GE11-P-{ichamber:02}L{ilayer}:TEMP:OLD:VALUE"] = _format_fos_query(f"cms_gem_dcs_1:GE11_p_{ichamber:02}_FOS", "GEMDIPFOS", f"L{ilayer}TEMP")
            tags[f"GE11-P-{ichamber:02}L{ilayer}:TEMP:OLD:VALID"] = _format_fos_query(f"cms_gem_dcs_1:GE11_p_{ichamber:02}_FOS", "GEMDIPFOS", f"L{ilayer}TEMPVALIDITY")

    # New GE1/1 Dps -- limited to odd (i.e. short) chambers
    for ichamber in range(1, 37, 2):
        for ilayer in range(1,3):
            tags[f"GE11-M-{ichamber:02}L{ilayer}:TEMP:VALUE"] = _format_fos_query(f"cms_gem_dcs_1:GE11_m_{ichamber:02}_L{ilayer}_FOS", "GEMDIPFOSTEMPERATURE", f"TEMP")
            tags[f"GE11-M-{ichamber:02}L{ilayer}:TEMP:VALID"] = _format_fos_query(f"cms_gem_dcs_1:GE11_m_{ichamber:02}_L{ilayer}_FOS", "GEMDIPFOSTEMPERATURE", f"VALIDITY")

            tags[f"GE11-P-{ichamber:02}L{ilayer}:TEMP:VALUE"] = _format_fos_query(f"cms_gem_dcs_1:GE11_p_{ichamber:02}_L{ilayer}_FOS", "GEMDIPFOSTEMPERATURE", f"TEMP")
            tags[f"GE11-P-{ichamber:02}L{ilayer}:TEMP:VALID"] = _format_fos_query(f"cms_gem_dcs_1:GE11_p_{ichamber:02}_L{ilayer}_FOS", "GEMDIPFOSTEMPERATURE", f"VALIDITY")

    # GE2/1 Dps -- limited to the production chambers
    for iendcap, ichamber, ilayer in (("M", 2, 1), ("M", 3, 1), ("M", 4, 1), ("M", 16, 1), ("M", 18, 1)):
        idcsendcap = iendcap.lower()
        for imodule in ("A", "B", "C", "D"):
            idcsmodule = ord(imodule) - ord("A") + 1

            tags[f"GE21-{iendcap}-{ichamber:02}L{ilayer}{imodule}:TEMP:VALUE"] = _format_fos_query(f"cms_gem_dcs_1:GE21_{idcsendcap}_{ichamber:02}_L{ilayer}_M{idcsmodule}_FOS", "GEMDIPFOSTEMPERATURE", f"TEMP")
            tags[f"GE21-{iendcap}-{ichamber:02}L{ilayer}{imodule}:TEMP:VALID"] = _format_fos_query(f"cms_gem_dcs_1:GE21_{idcsendcap}_{ichamber:02}_L{ilayer}_M{idcsmodule}_FOS", "GEMDIPFOSTEMPERATURE", f"VALIDITY")

    return tags

MAGNET_QUERY = """
-- Select data within the time window
SELECT
    event.CHANGE_DATE AS timestamp,
    event."{in_event_column_name}" as value

FROM CMS_GEM_PVSS_COND.GEMDIPMAGNETINFO event

JOIN CMS_GEM_PVSS_COND.DP_NAME2ID dp
    ON event.DPID = dp.ID

WHERE
    dp.DPNAME = '{in_dp_name}'
    AND event."{in_event_column_name}" IS NOT NULL
    AND event.CHANGE_DATE >= :var_from_date
    AND event.CHANGE_DATE < :var_to_date

ORDER BY event.CHANGE_DATE ASC
"""

def _format_magnet_query(*params):
    formatted_query = MAGNET_QUERY.format(
        in_dp_name = params[0],
        in_event_column_name = params[1],
    )
    return formatted_query

def _fill_magnet_metrics():
    tags = dict()

    tags[f"FIELD"] = _format_magnet_query("cms_gem_dcs_1:magnetStatus", "FIELD")
    tags[f"CURRENT"] = _format_magnet_query("cms_gem_dcs_1:magnetStatus", "CURRENT")

    tags[f"CRYO"] = _format_magnet_query("cms_gem_dcs_1:magnetStatus", "CRYOSTS")
    tags[f"STEADY"] = _format_magnet_query("cms_gem_dcs_1:magnetStatus", "STEADYSTS")
    tags[f"RAMPING"] = _format_magnet_query("cms_gem_dcs_1:magnetStatus", "RAMPINGSTS")
    tags[f"EMERGENCY"] = _format_magnet_query("cms_gem_dcs_1:magnetStatus", "EMERGENCYSTS")

    tags[f"MANUAL"] = _format_magnet_query("cms_gem_dcs_1:magnetStatus", "MANUALMAGNETPROTECTION")
    tags[f"TRIGGERED"] = _format_magnet_query("cms_gem_dcs_1:magnetStatus", "TRIGGERMAGNETPROTECTION")

    return tags

GAS_QUERY = """
-- Select data within the time window
SELECT
    event.CHANGE_DATE AS timestamp,
    event.VALUE as value

FROM CMS_GEM_PVSS_COND.{in_event_table_name} event

JOIN CMS_GEM_PVSS_COND.DP_NAME2ID dp
    ON event.DPID = dp.ID

WHERE
    dp.DPNAME = '{in_dp_name}'
    AND event.VALUE IS NOT NULL
    AND event.CHANGE_DATE >= :var_from_date
    AND event.CHANGE_DATE < :var_to_date

ORDER BY event.CHANGE_DATE ASC
"""

def _format_gas_query(*params):
    formatted_query = GAS_QUERY.format(
        in_dp_name = params[0],
        in_event_table_name = params[1],
    )
    return formatted_query

def _fill_gas_metrics():
    tags = dict()

    # PLC
    tags[f"PLC:ALIVE_COUNTER"] = _format_gas_query("cms_gem_dcs_1:CMSGEM_PlcCounter", "DipSubscriptionsInt")

    # Sytem
    tags[f"SYSTEM:STATE"] = _format_gas_query("cms_gem_dcs_1:CMSGEM_Gs_GsStepWS", "DipSubscriptionsInt")
    tags[f"SYSTEM:STATUS"] = _format_gas_query("cms_gem_dcs_1:CMSGEM_Gs_GSY0505", "DipSubscriptionsBool")

    # Mixer
    tags[f"MIXER:OUT_PRESSURE"] = _format_gas_query("cms_gem_dcs_1:CMSGEM_Mx_PT1009", "DipSubscriptionsFloat")
    tags[f"MIXER:STATE"] = _format_gas_query("cms_gem_dcs_1:CMSGEM_Mx_StepperWS", "DipSubscriptionsInt")
    tags[f"MIXER:TOT_MFC"] = _format_gas_query("cms_gem_dcs_1:CMSGEM_Mx_TotalFlowAS", "DipSubscriptionsFloat")

    for iLine, iGas in [(1, "AR"), (2, "CO2")]:
        tags[f"MIXER:{iGas}:IN_PRESSURE"] = _format_gas_query(f"cms_gem_dcs_1:CMSGEM_Mx_PT1{iLine}03", "DipSubscriptionsFloat")
        tags[f"MIXER:{iGas}:MFC"] = _format_gas_query(f"cms_gem_dcs_1:CMSGEM_Mx_XMFC1{iLine}06", "DipSubscriptionsFloat")
        tags[f"MIXER:{iGas}:RATIO"] = _format_gas_query(f"cms_gem_dcs_1:CMSGEM_Mx_L{iLine}CompRatioAS", "DipSubscriptionsFloat")

    # Distribution
    tags[f"DISTRIBUTION:STATE"] = _format_gas_query(f"cms_gem_dcs_1:CMSGEM_Di_ModStepWS", "DipSubscriptionsInt")
    tags[f"DISTRIBUTION:TOT_IN_FLOW"] = _format_gas_query(f"cms_gem_dcs_1:CMSGEM_Di_TotFlowAS", "DipSubscriptionsFloat")

    tags[f"DISTRIBUTION:GROUP1:PRE_OUT_PRESSURE"] = _format_gas_query("cms_gem_dcs_1:CMSGEM_Di_PT6128", "DipSubscriptionsFloat")
    tags[f"DISTRIBUTION:GROUP2:PRE_OUT_PRESSURE"] = _format_gas_query("cms_gem_dcs_1:CMSGEM_Di_PT6428", "DipSubscriptionsFloat")

    for iRack, nChannels in [(61, 13), (62, 18), (64, 13), (65, 18)]:
        tags[f"DISTRIBUTION:RACK{iRack}:IN_PRESSURE"] = _format_gas_query(f"cms_gem_dcs_1:CMSGEM_Di_PT{iRack}24", "DipSubscriptionsFloat")
        tags[f"DISTRIBUTION:RACK{iRack}:OUT_PRESSURE"] = _format_gas_query(f"cms_gem_dcs_1:CMSGEM_Di_PT{iRack}25", "DipSubscriptionsFloat")
        tags[f"DISTRIBUTION:RACK{iRack}:PRE_IN_PRESSURE"] = _format_gas_query(f"cms_gem_dcs_1:CMSGEM_Di_PT{iRack}22", "DipSubscriptionsFloat")
        tags[f"DISTRIBUTION:RACK{iRack}:STATE"] = _format_gas_query(f"cms_gem_dcs_1:CMSGEM_Di_Rack{iRack}StepWS", "DipSubscriptionsInt")

        for iChannel in range(1, nChannels + 1):
            tags[f"DISTRIBUTION:RACK{iRack}:CHANNEL{iChannel}:IN_FLOW"] = _format_gas_query(f"cms_gem_dcs_1:CMSGEM_Di_FE{iRack}02Ch{iChannel}", "DipSubscriptionsFloat")
            tags[f"DISTRIBUTION:RACK{iRack}:CHANNEL{iChannel}:OUT_FLOW"] = _format_gas_query(f"cms_gem_dcs_1:CMSGEM_Di_FE{iRack}06Ch{iChannel}", "DipSubscriptionsFloat")

    # Pumps
    tags[f"PUMP:CONTROL_VALVE"] = _format_gas_query("cms_gem_dcs_1:CMSGEM_Pp_PV4003", "DipSubscriptionsFloat")
    tags[f"PUMP:DIFF_PRESSURE"] = _format_gas_query("cms_gem_dcs_1:CMSGEM_Pp_PDT4020", "DipSubscriptionsFloat")
    tags[f"PUMP:IN_PRESSURE"] = _format_gas_query("cms_gem_dcs_1:CMSGEM_Pp_PT4004", "DipSubscriptionsFloat")
    tags[f"PUMP:OUT_PRESSURE"] = _format_gas_query("cms_gem_dcs_1:CMSGEM_Pp_PT4006", "DipSubscriptionsFloat")
    tags[f"PUMP:SET_PRESSURE"] = _format_gas_query("cms_gem_dcs_1:CMSGEM_Pp_RnDfltPSp", "DipSubscriptionsFloat")
    tags[f"PUMP:STATE"] = _format_gas_query("cms_gem_dcs_1:CMSGEM_Pp_StepperWS", "DipSubscriptionsInt")

    for iPump in [1, 2]:
        tags[f"PUMP:PUMP{iPump}:PRESSURE"] = _format_gas_query(f"cms_gem_dcs_1:CMSGEM_Pp_PT4{iPump}01_1", "DipSubscriptionsFloat")
        tags[f"PUMP:PUMP{iPump}:RUN_TIME"] = _format_gas_query(f"cms_gem_dcs_1:CMSGEM_Pp_RunTime{iPump}AS", "DipSubscriptionsFloat")
        tags[f"PUMP:PUMP{iPump}:TEMPERATURE"] = _format_gas_query(f"cms_gem_dcs_1:CMSGEM_Pp_TE4{iPump}01_1", "DipSubscriptionsFloat")

    return tags

METRICS = {
    "iMon": {**_fill_lv_metrics("ACTUAL_IMON"), **_fill_hv_metrics("ACTUAL_IMON")},
    "vMon": {**_fill_lv_metrics("ACTUAL_VMON"), **_fill_hv_metrics("ACTUAL_VMON")},
    "isOn": _fill_hv_metrics("ACTUAL_ISON"),
    "status": _fill_hv_metrics("ACTUAL_STATUS"),
    "i0Set": _fill_hv_metrics("READBACKSETTINGS_I0"),
    "v0Set": _fill_hv_metrics("READBACKSETTINGS_V0"),
    "rack": _fill_rack_metrics(),
    "fos": _fill_fos_metrics(),
    "magnet": _fill_magnet_metrics(),
    "gas": _fill_gas_metrics(),
}
