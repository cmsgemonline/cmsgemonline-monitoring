# GEM DCS DB to Grafana/JSON bridge

This simple application aims at exposing the GEM DCS archive DB information as JSON objects understandable by the [`simpod-json-datasource`](https://github.com/simPod/GrafanaJsonDatasource) Grafana Datasource.

## Usage

* Installation: `poetry install`
* Configuration: `export DCS_DB_GRAFANA_BRIDGE_DB="<gem-dcs-conditions-read-credentials>"`
* Run: `poetry run gunicorn`

## A few more details...

The supported metrics (e.g. `iMon`) are directly exposed in the Grafana GUI.

Additional filtering is implemented via hierarchical tags, which can be retrieved as a Grafana variables.
The colon character (`:`) is used as the hierarchy separator.
Tags can and should be provided for each query as a JSON lists in the query builder field.
**Note that only fully qualified tags can be provided. Partial tags are not supported.**

A target named `<metric>,<fully-qualified-tag>` will be returned for each (metric, tag) pair queried.
